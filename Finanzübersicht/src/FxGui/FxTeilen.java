package FxGui;

import java.time.LocalDate;

import Datenbank.BenutzerSQL;
import Datenbank.BuchungSQL;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import logic.Benutzer;
import logic.Buchung;

public class FxTeilen {
	static String benutzer;
	static String Finalerbetrag;
	static String text;
	static VBox vbox;
	static int betrag; 
	static DatePicker datePicker;
	static String datum;
	public static Scene teilenScene() {
		BorderPane border = new BorderPane();
		GridPane grid = new GridPane();
		ListView listView = new ListView();
		listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		for (String test : BenutzerSQL.nutzerAusgeben()) {
			listView.getItems().add(test);
		}
		// hier kommt die methode die die Benutzer von der Liste hinzufügt
		listView.getItems().add("lkdaleo");
		listView.getItems().add("sjdjn");
		listView.getItems().add("lkdalgfeo");
		listView.getItems().add("hzjhgf");
		listView.getItems().add("gfdewee");
		TextField textfieldb = new TextField("");
		TextField textfieldt = new TextField("");
		datePicker = new DatePicker();
		Button button = new Button("Benutzer auswählen");
		grid.add(new Label("Betrag:"),0,1);
		grid.add(textfieldb, 0, 2);
		grid.add(new Label("Text: "), 0, 3);
		grid.add(textfieldt, 0, 4);
		grid.add(new Label("Datum: "), 0, 5);
		grid.add(datePicker, 0, 6);
		grid.add(button, 0, 7);
		
		
		
		String datum2 = datum;
		
		

		vbox = new VBox();
		ObservableList elementAnzeigen = listView.getSelectionModel().getSelectedItems();
		
		button.setOnAction(action -> {
			betrag = Integer.parseInt(textfieldb.getText());
			int zubezahlen = betrag /elementAnzeigen.size() ;
			Finalerbetrag = Integer.toString(zubezahlen);
			ColumnConstraints j = new ColumnConstraints();
		datum = datePicker.getValue().toString();
			
			
			for (int i = elementAnzeigen.size(); i > 0; i--) {
				benutzer = (String)elementAnzeigen.get(i - 1);
				text =textfieldt.getText();
				
				HBox tmphBox = new HBox();
				
				Separator verseparator = new Separator();
				verseparator.setOrientation(Orientation.VERTICAL);
				verseparator.setPrefHeight(35);
				
				Separator verseparator2 = new Separator();
				verseparator2.setOrientation(Orientation.VERTICAL);
				verseparator2.setPrefHeight(35);
				
				Separator verseparator3 = new Separator();
				verseparator3.setOrientation(Orientation.VERTICAL);
				verseparator3.setPrefHeight(35);
				
				tmphBox.getChildren().addAll(new Label("Benutzername: " ));
				tmphBox.getChildren().addAll((new Label(benutzer )));
				tmphBox.getChildren().add(verseparator);
				
				tmphBox.getChildren().addAll(new Label("Betrag: "));
				tmphBox.getChildren().addAll(new Label(Finalerbetrag));
				tmphBox.getChildren().add(verseparator2);
				
				tmphBox.getChildren().add(new Label("Text: "));
				tmphBox.getChildren().addAll(new Label(text));
				tmphBox.getChildren().add(verseparator3);
				
				tmphBox.getChildren().add(new Label("Datum: "));
				tmphBox.getChildren().addAll(new Label(datum));
				
				
				transaktionspeichern();
				
				Separator separator = new Separator();
				separator.setMaxWidth(1000);
				
				vbox.getChildren().add(tmphBox);
				vbox.getChildren().add(separator);
				
			}
		
			Stage nächstesFenster = new Stage();
			nächstesFenster.setScene(FxTeilen.rechnungTeilen());
			nächstesFenster.show();
		});
		border.setCenter(listView);
		border.setRight(grid);
		//VBox vBox = new VBox(listView, button,grid);
		Scene scene = new Scene(border, 300, 300);
		return scene;
	}

	public static Scene rechnungTeilen() {
		
		VBox vBox = new VBox(vbox);
		Scene mainWindowScene = new Scene(vBox, 600, 500);
		return mainWindowScene;
	}
	
	public static void transaktionspeichern() {
		Benutzer benutzer2 = new Benutzer(benutzer);
		Double betrag2 = Double.valueOf(Finalerbetrag);
		Buchung Buchung = new Buchung(benutzer2, datum, betrag2, text);
		BuchungSQL.buchen(Buchung);
		System.out.println(benutzer + Finalerbetrag + text + datum);
	}
}
