package FxGui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import logic.Benutzer;
import Datenbank.BenutzerSQL;
public class FxLoggin extends Application {

	Alert alert;
	private String pfReg3;
	private TextField TFR;
	private TextField TFL;
	private PasswordField pflogin;
	private PasswordField pfReg1;
	private PasswordField pfReg2;

	public Scene mainWindowScene;
	Stage window;
	Scene scene;

	public static void main(String[] args) {
		launch(args);
	}

	public void start(Stage stage) {
		window = stage;
		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		Tab tabLogin = new Tab("Loggin");
		Tab tabRegistrieren = new Tab("Registrieren");
		tabPane.getTabs().add(tabLogin);
		tabPane.getTabs().add(tabRegistrieren);
		// -----------------Login Tab-----------------------//
		GridPane benutzerDaten = new GridPane();
		TFL = new TextField();
		 pflogin = new PasswordField();
		benutzerDaten.add(new Label("Benutzername :"), 0, 1);
		benutzerDaten.add(TFL, 1, 1);
		benutzerDaten.add(new Label("Passwort: "), 0, 2);
		benutzerDaten.add(pflogin, 1, 2);
		tabLogin.setContent(benutzerDaten);
		// --------------Registrieren Tab-------------------//
		GridPane registrieren = new GridPane();
		TFR = new TextField();
		pfReg1 = new PasswordField();
		pfReg2 = new PasswordField();
		registrieren.add(new Label("Benutzername :"), 0, 1);
		registrieren.add(TFR, 1, 1);
		registrieren.add(new Label("Passwort :"), 0, 2);
		registrieren.add(pfReg1, 1, 2);
		registrieren.add(new Label("Passwort Wiederholen :"), 0, 3);
		registrieren.add(pfReg2, 1, 3);
		tabRegistrieren.setContent(registrieren);

		BorderPane border = new BorderPane();
		FlowPane flow = new FlowPane();
		FlowPane flow2 = new FlowPane();
		Button bAnmelden = new Button("Anmelden");
		Button bRegistrieren = new Button("Registrieren");
		flow.getChildren().add(bAnmelden);
		flow.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
		flow2.getChildren().add(bRegistrieren);
		flow2.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
		border.setBottom(flow);
		Tooltip tooltip1 = new Tooltip("Zum anmelden");
		Tooltip tooltip2 = new Tooltip("Benutzernamen eingeben");
		bAnmelden.setTooltip(tooltip1);
		TFL.setTooltip(tooltip2);

		tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
				if (newValue == tabRegistrieren) {
					border.setBottom(flow2);
				} else if (newValue == tabLogin) {
					border.setBottom(flow);
				}
			}
		});

		bAnmelden.setOnAction(action -> {
			Benutzer b = new Benutzer(TFL.getText(),pflogin.getText());
			Benutzer a = new Benutzer(TFL.getText());
			oeffentlicherBenutzer = a;
		if(BenutzerSQL.ueberpruefeBenutzer(b)!= null) {
			buttonclicklogin();
			neufenster();
		}else {
			System.out.println("Nicht vorhanden");
		}
			
		});

		bRegistrieren.setOnAction(action -> {
			if (pfReg1.getText().equals(pfReg2.getText())) {
				pfReg3 = pfReg1.getText();
				System.out.println(pfReg3);
				buttonclickreg();
				neufenster();
				stage.close();
			} else {
				alert = new Alert(AlertType.ERROR);
				alert.setHeaderText("Die Passw�rter sind nicht gleich!");
				alert.setContentText("Geben Sie das Passwort nochmal ein");
				alert.showAndWait().ifPresent(gedr�ckt -> {
					if (gedr�ckt == ButtonType.OK) {
						alert.close();
					}
				});
				System.out.println();
			}
			

		});

		border.setTop(tabPane);
		border.setCenter(benutzerDaten);
		scene = new Scene(border, 400, 200);
		stage.setScene(scene);
		stage.show();
	}

	public void buttonclicklogin() {
		String benutzer = TFL.getText();
		String passwort = pflogin.getText();
		Benutzer benutzerL = new Benutzer(benutzer, passwort);
		BenutzerSQL.ueberpruefeBenutzer(benutzerL);
		
	}
	
	public void buttonclickreg() {
		String B = TFR.getText();
		String Passwort = pfReg1.getText();
		String Passwort2 = pfReg2.getText();
		Benutzer benutzerR = new Benutzer(B, Passwort, Passwort2);
		BenutzerSQL.addUser(benutzerR);
		System.out.println(benutzerR);
	}

	public static void neufenster() {
		Stage newWindow = new Stage();
		newWindow.setScene(Fxuebersicht.uebersichtScene());
		newWindow.show();
	}
	public static Benutzer oeffentlicherBenutzer;
}
