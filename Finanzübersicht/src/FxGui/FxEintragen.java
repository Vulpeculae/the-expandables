package FxGui;

import javafx.geometry.NodeOrientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import logic.Benutzer;
import logic.Buchung;
import Datenbank.BuchungSQL;

public class FxEintragen {
	static DatePicker datePicker;
	static TextField textField;
	static TextField textField2;
	public static Scene eintragenScene() {
		GridPane grid = new GridPane();
		datePicker = new DatePicker();
		grid.add(new Label("Datum: "),0,1);
		grid.add(datePicker,0,2);
		
		textField = new TextField();
		grid.add(new Label("Betrag: "),1,1);
		grid.add(textField,1,2);
		
		textField2 = new TextField();
		grid.add(new Label("Herkunft des Betrags: "), 2, 1);
		grid.add(textField2,2,2);
		
		FlowPane flow = new FlowPane(4,4);
		Button button = new Button("Eintragen");
		flow.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
		flow.getChildren().add(button);
		
		button.setOnAction(action -> {
			eintragen();
			String datum = datePicker.getValue().toString();
			 String betrag = textField.getText();
			 double betragdouble = Double.parseDouble(betrag);
			 String text = textField2.getText();
			 Benutzer benutzer = FxLoggin.oeffentlicherBenutzer;
			 Buchung buchung = new Buchung(benutzer,datum,betragdouble,text);
			BuchungSQL.raufBuchen(buchung.getBenutzer_id(), buchung.getBetrag());
		});
		
		BorderPane border = new BorderPane();
		border.setCenter(grid);
		border.setBottom(flow);
		Scene scene = new Scene(border, 595,84);
		return scene;
	}
	public static void eintragen() {
		 String datum = datePicker.getValue().toString();
		 String betrag = textField.getText();
		 double betragdouble = Double.parseDouble(betrag);
		 String text = textField2.getText();
		 Benutzer benutzer = FxLoggin.oeffentlicherBenutzer;
		 Buchung buchung = new Buchung(benutzer,datum,betragdouble,text);
		 BuchungSQL.buchen(buchung);
		 
	}

}
