package FxGui;

import Datenbank.BenutzerSQL;
import Datenbank.BuchungSQL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.NodeOrientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import logic.Benutzer;
import logic.Buchung;

public class FxSpenden {
	static TextField textField;
	static TextField textField2;
	static ComboBox<String> combo;
	static DatePicker datePicker;
	public static Scene spendenScene() {
		ObservableList<String> benutzernamen = FXCollections.observableArrayList(BenutzerSQL.nutzerAusgeben() );
		combo = new ComboBox<String>(benutzernamen);
		
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.getColumnConstraints().add(new ColumnConstraints(150));
		grid.add(new Label("Benutzername: "), 0, 1);
		grid.add(combo, 0, 2);
		
		
		textField = new TextField();
		grid.add(new Label("Betrag: "),1,1);
		grid.add(textField,1,2);
		
		textField2 = new TextField();
		grid.add(new Label("Grund des Spenden: "), 2, 1);
		grid.add(textField2,2,2);
		
		datePicker = new DatePicker();
		grid.add(new Label("Datum: "),3,1);
		grid.add(datePicker,3,2);
		
		Button button = new Button("Abschicken");
		FlowPane flow = new FlowPane(1,1);
		flow.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
		flow.getChildren().add(button);
		
		button.setOnAction(action -> {
			geldsenden();
		});
		BorderPane border = new BorderPane();
		border.setCenter(grid);
		border.setBottom(flow);
		Scene scene = new Scene(border, 595,84);
		return scene;
	}
	
	public static void geldsenden() {
		 Benutzer benutzer = FxLoggin.oeffentlicherBenutzer;
		String benutzername = combo.getValue();
		Double betrag = Double.valueOf(textField.getText());
		String text = textField.getText();
		String datum = datePicker.getValue().toString();
		Buchung buchung = new Buchung(benutzer, datum,betrag, text );
		BuchungSQL.geldSenden(buchung, benutzername);
	}
}
