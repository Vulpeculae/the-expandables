package FxGui;

import java.util.ArrayList;

import Datenbank.BuchungSQL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import logic.Buchung;

public class Fxuebersicht {
	static TextField textfieldF;
	static Button buttonfilter;

	public static Scene uebersichtScene() {
		HBox hbox = new HBox();
		BorderPane border = new BorderPane();

		GridPane grid = new GridPane();
		textfieldF = new TextField();
		buttonfilter = new Button("Filtern");

		grid.add(new Label("Filter: "), 0, 1);
		grid.add(textfieldF, 1, 1);
		grid.add(buttonfilter, 2, 1);

		TableView<Buchung> table = new TableView<Buchung>();
		ArrayList<Buchung> buchungen = BuchungSQL.buchungenAusgeben(FxLoggin.oeffentlicherBenutzer);
		for (int i = 0; i <buchungen.size(); i++) {
			
			table.getItems().add(i, buchungen.get(i));
		}
		
		System.out.println(buchungen.toString());
		ObservableList<Buchung> data =
		 FXCollections.observableArrayList(new Buchung("10.05.2018",300,"Beispiel"));

		TableColumn<Buchung, String> spalteDatum = new TableColumn<Buchung, String>("Datum");
		spalteDatum.setMinWidth(100);
		spalteDatum.setCellValueFactory(new PropertyValueFactory<Buchung, String>("Datum"));
		table.getColumns().add(spalteDatum);

		TableColumn<Buchung, String> spalteBetrag = new TableColumn<Buchung, String>("Betrag");
		spalteBetrag.setMinWidth(100);
		spalteBetrag.setCellValueFactory(new PropertyValueFactory<Buchung, String>("Betrag"));
		table.getColumns().add(spalteBetrag);

		TableColumn<Buchung, String> spalteQuelle = new TableColumn<Buchung, String>("Quelle");
		spalteQuelle.setMinWidth(100);
		spalteQuelle.setCellValueFactory(new PropertyValueFactory<Buchung, String>("Quelle"));
		table.getColumns().add(spalteQuelle);

		Button bTeilen = new Button("Rechnung teilen");
		hbox.getChildren().add(bTeilen);
		hbox.getChildren().add(new Separator());

		Button bEintragen = new Button("Buchung eintragen");
		hbox.getChildren().add(bEintragen);
		hbox.getChildren().add(new Separator());

		Button bSpenden = new Button("Geld Spenden");
		hbox.getChildren().add(bSpenden);

		bEintragen.setOnAction(action -> {
			Stage eintragen�ffnen = new Stage();
			eintragen�ffnen.setScene(FxEintragen.eintragenScene());
			eintragen�ffnen.show();
		});
		bSpenden.setOnAction(action -> {
			Stage spenden�ffnen = new Stage();
			spenden�ffnen.setScene(FxSpenden.spendenScene());
			spenden�ffnen.show();
		});
		bTeilen.setOnAction(action -> {
			Stage teilen�ffnen = new Stage();
			teilen�ffnen.setScene(FxTeilen.teilenScene());
			teilen�ffnen.show();
		});
		buttonfilter.setOnAction(action -> {
			filtern();
			FxLoggin.neufenster();
			BuchungSQL.buchungenAusgeben(FxLoggin.oeffentlicherBenutzer);
		});
		Scene scene = new Scene(border, 400, 200);
		border.setTop(grid);
		border.setCenter(table);
		border.setBottom(hbox);
		return scene;
	}
	public static void filtern() {
		String suche = textfieldF.getText();
		BuchungSQL.filterBuchungen(suche);
	}
}
