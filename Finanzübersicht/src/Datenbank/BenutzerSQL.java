package Datenbank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import logic.Benutzer;
import logic.Buchung;

public class BenutzerSQL extends SQL{
	
	public static boolean addUser(Benutzer benutzer) {
		Connection con = null;
		PreparedStatement stmt = null;
		//test
		try {
			
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");
			
			String query = "INSERT INTO T_Benutzer(P_Benutzer_ID, Kontostand, Passwort_Hash, Passwort_Salt) VALUES (?, ?, ?, ?)";
			
			stmt = con.prepareStatement(query);
			stmt.setString(1, benutzer.getBenutzername());
			stmt.setDouble(2,0);
			stmt.setString(3, benutzer.getPasswortSalt());
			stmt.setString(4, benutzer.getPasswortSalt());
			
			stmt.execute();
			con.commit();
			return true;
		}catch(SQLException e1) {
			try {
				e1.printStackTrace();
				con.rollback();
			} catch (SQLException e) {}
			return false;
		}finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	//checkUser returns null if user doesn't exist.
	public static Benutzer ueberpruefeBenutzer(Benutzer benutzer) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");
			
			String query = "Select P_Benutzer_ID, passwort_hash, passwort_salt From T_benutzer WHERE P_Benutzer_ID = ? ";
			
			stmt = con.prepareStatement(query);
			stmt.setString(1, benutzer.getBenutzername());
			//stmt.setString(2, benutzer.getBenutzername());
			
			
			ResultSet rs = stmt.executeQuery();
			con.commit();
			if(!rs.next()) {
				System.out.println("Benutzer exestiert nicht");
				return null;
			}else {			
				System.out.println("Eingeloggt");
				return new Benutzer(rs.getString(1), rs.getString(2));
			}
		}catch(SQLException e1) {
			try {
				con.rollback();
				System.out.println(e1);
			} catch (SQLException e) {}
			return null;
		}
	}
	public static ArrayList<String> nutzerAusgeben() {                     //gibt die Nutzer nur als String aus
		ArrayList<String> Benutzerliste = new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");
			
			String query = "Select * From T_Benutzer";
			
			stmt = con.prepareStatement(query);
			
			
			ResultSet rs = stmt.executeQuery();
			con.commit();
			while(rs.next()) {
				
			 Benutzerliste.add(rs.getString("P_Benutzer_ID"));
			}
			return Benutzerliste;
		}catch(SQLException e1) {
			try {
				con.rollback();
				System.out.println(e1);
			} catch (SQLException e) {}
			return null;
		}
}}

