package Datenbank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import logic.Benutzer;
import logic.Buchung;

public class BuchungSQL extends SQL {

	public static boolean raufBuchen(String p_benutzer_id, double betrag) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");

			String query = "UPDATE T_Benutzer SET Kontostand = Kontostand + (?) WHERE p_benutzer_id = (?);";

			stmt = con.prepareStatement(query);
			stmt.setDouble(1, betrag);
			stmt.setString(2, p_benutzer_id);

			stmt.execute();
			con.commit();
			return true;
		} catch (SQLException e1) {
			try {
				e1.printStackTrace();
				con.rollback();
			} catch (SQLException e) {
			}
			return false;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public static ArrayList<Buchung> filterBuchungen(String begriff) {
		ArrayList<Buchung> Buchungen = new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");

			String query = "Select * From T_Buchung WHERE( P_Buchungs_ID = ? or Datum = ? or Betrag = ? or Quelle = ?)";

			stmt = con.prepareStatement(query);
			stmt.setString(1, begriff);
			stmt.setString(2, begriff);
			stmt.setString(3, begriff);
			stmt.setString(4, begriff);

			ResultSet rs = stmt.executeQuery();
			con.commit();
			while (rs.next()) {
				Buchung buchung = new Buchung(rs.getInt("P_Buchungs_ID"), rs.getString("F_Absender_ID"),
						rs.getString("Datum"), rs.getDouble("Betrag"), rs.getString("Quelle"));
				Buchungen.add(buchung);
			}
			return Buchungen;
		} catch (SQLException e1) {
			try {
				con.rollback();
				System.out.println(e1);
			} catch (SQLException e) {
			}
			return null;
		}
	}

	public static boolean rechnungTeilen() {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");

			String query = "Select P_Buchungs_ID, Quelle, Betrag, Datum From T_Benutzer WHERE P_Buchungs_ID = ? ";

			stmt = con.prepareStatement(query);

			ResultSet rs = stmt.executeQuery();
			con.commit();
			if (!rs.next()) {
				System.out.println("Benutzer existiert nicht");
				return false;
			} else {
				System.out.println("Benutzer existiert");
				return false;
			}
		} catch (SQLException e1) {
			try {
				con.rollback();
				System.out.println(e1);
			} catch (SQLException e) {
			}
			return false;
		}
	}

	public static boolean geldSenden(Buchung buchung, String benutzer_id) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");

			String query = "INSERT INTO T_Buchung (f_absender_id, quelle, betrag, datum) VALUES (?, ?, ?,?); INSERT INTO T_benutzer_buchungen (PF_benutzer_id, PF_buchungs_id,ist_absender,Betrag, Quelle, Datum) VALUES (?, LAST_INSERT_ID(),?,?,?,?); INSERT INTO T_benutzer_buchungen (PF_benutzer_id, PF_buchungs_id,ist_absender,Betrag, Quelle, Datum) VALUES (?, Last_INSERT_ID(),?,?,?,?);";
			stmt = con.prepareStatement(query);
			stmt.setString(1, buchung.getBenutzer().getBenutzername());
			stmt.setString(2, buchung.getQuelle());
			stmt.setDouble(3, buchung.getBetrag());
			stmt.setString(4, buchung.getDatum());

			stmt.setString(5, buchung.getBenutzer().getBenutzername());
			stmt.setBoolean(6, true);
			stmt.setDouble(7, buchung.getBetrag());
			stmt.setString(8, buchung.getQuelle());
			stmt.setString(9, buchung.getDatum());

			stmt.setString(10, benutzer_id);
			stmt.setBoolean(11, false);
			stmt.setDouble(12, buchung.getBetrag());
			stmt.setString(13, buchung.getQuelle());
			stmt.setString(14, buchung.getDatum());

			stmt.execute();
			con.commit();

			String query2 = "INSERT INTO T_benutzer_buchungen (PF_benutzer_id, PF_buchungs_id,ist_absender,Betrag, Quelle, Datum) VALUES (?, ?, ?,?, ?,? )  ";

			stmt.execute();
			con.commit();
			return true;
		} catch (SQLException e1) {
			try {
				e1.printStackTrace();
				con.rollback();
			} catch (SQLException e) {
			}
			return false;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public static boolean buchen(Buchung buchung) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog("finanzübersicht");

			String query = "INSERT INTO t_buchung (F_Absender_id, quelle, betrag, datum) VALUES (?, ?, ?,?);  ";

			stmt = con.prepareStatement(query);
			stmt.setString(1, buchung.getBenutzer().getBenutzername());
			stmt.setString(2, buchung.getQuelle());
			stmt.setDouble(3, buchung.getBetrag());
			stmt.setString(4, buchung.getDatum());

			stmt.execute();
			con.commit();
			return true;
		} catch (SQLException e1) {
			try {
				e1.printStackTrace();
				con.rollback();
			} catch (SQLException e) {
			}
			return false;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	public static ArrayList<Buchung> buchungenAusgeben(Benutzer benutzer) {
        ArrayList<Buchung> Buchungen = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            con.setCatalog("finanzübersicht");
            String query = "Select Quelle, Betrag, Datum From t_buchung where F_Absender_ID = ? ";

            stmt = con.prepareStatement(query);
			stmt.setString(1, benutzer.getBenutzername());
			

			stmt.execute();
			con.commit();
			

			ResultSet rs = stmt.executeQuery();
			con.commit();
			while (rs.next()) {
            
            
            
            while(rs.next()) {
            	System.out.println("Test");
                Buchung buchung = new Buchung(rs.getString("Datum"), rs.getDouble("Betrag"), rs.getString("Quelle"));
                Buchungen.add(buchung);
            }
            return Buchungen;
			} }catch(SQLException e1) {
        	e1.printStackTrace();
            try {
                con.rollback();
                System.out.println(e1);
            } catch (SQLException e) {}
            return null;
        }
		return Buchungen;
    }
}
