package logic;

public class Benutzer {
	private String Benutzername;
	private double Kontostand = 0;
	private String Passwort;
	private String PasswortHash;
	private String PasswortSalt;
	
	public Benutzer(String benutzername, double kontostand, String passwortHash, String passwortSalt) {
		this.Benutzername = benutzername;
		this.Kontostand = kontostand;
		this.PasswortHash = passwortHash;
		this.PasswortSalt = passwortSalt;
	}
	public Benutzer(String benutzername, String passwortHash, String passwortSalt) {
		this.Benutzername = benutzername;
		this.PasswortHash = passwortHash;
		this.PasswortSalt = passwortSalt;
	}
	public Benutzer(String benutzername, double kontostand) {
		this.Benutzername = benutzername;
		this.Kontostand = kontostand;
	}
	public Benutzer(String benutzername) {
		this.Benutzername = benutzername;
	
	}
	public Benutzer(String Benutzername, String Passwort) {
		this.Benutzername = Benutzername;
		this.Passwort = Passwort;
	}
	public String getPasswort() {
		return Passwort;
	}

	public String getBenutzername() {
		return Benutzername;
	}

	public void setBenutzername(String benutzername) {
		Benutzername = benutzername;
	}

	public String getPasswortHash() {
		return PasswortHash;
	}

	public String getPasswortSalt() {
		return PasswortSalt;
	}
	public double getKontostand() {
		return Kontostand;
	}
	public void setKontostand(double kontostand) {
		Kontostand = kontostand;
	}

}
