package logic;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswortHash {
	private byte[] hash = null;
	
	public PasswortHash(String password, byte[] salt) {
		if(salt == null) {
			SecureRandom random = new SecureRandom();
			salt = new byte[32];
			random.nextBytes(salt);
		}
		
		
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
		try {
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			hash = f.generateSecret(spec).getEncoded();
			
		} catch (NoSuchAlgorithmException e) {
			System.out.println("PBKDF2WithHmacSHA512 ist auf diesem System nicht verf�gbar, wird aber ben�tigt. Bitte auf Java 8 updaten.");
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean equals(Object other){
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof PasswortHash)) {
	    	return false;
	    }else {
	    	if (new String(((PasswortHash) other).getHash()).equals(new String(getHash()))) {
	    		return true;
	    	}else {
	    		return false;
	    	}
	    }
	}
	
	public byte[] getHash() {
		return hash;
	}
}
