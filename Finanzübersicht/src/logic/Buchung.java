package logic;

public class Buchung {
	public Benutzer benutzer;
	public Benutzer getBenutzer() {
		return benutzer;
	}
	public void setBenutzer(Benutzer benutzer) {
		this.benutzer = benutzer;
	}
	public String benutzer2;
	public int Buchungs_ID;
	public String benutzer_id;
	public String Quelle;
	public double Betrag;
	public String Datum;

	public Buchung(String Datum, double Betrag, String Quelle) {
		this.Datum = Datum;
		this.Betrag = Betrag;
		this.Quelle = Quelle;
	}
	public Buchung(Benutzer benutzer, String Datum, double Betrag, String Quelle) {
		
		this.benutzer = benutzer;
		this.Datum = Datum;
		this.Betrag = Betrag;
		this.Quelle = Quelle;
	}
	public Buchung(int Buchungs_ID, String benutzer_id, String Datum, double Betrag, String Quelle) {
		this.Buchungs_ID = Buchungs_ID;
		this.benutzer_id = benutzer_id;
		this.Datum = Datum;
		this.Betrag = Betrag;
		this.Quelle = Quelle;
	}
public Buchung(String benutzer2, String Datum, double Betrag, String Quelle) {
		
		this.benutzer2 = benutzer2;
		this.Datum = Datum;
		this.Betrag = Betrag;
		this.Quelle = Quelle;
	}
	public int getBuchungs_ID() {
		return Buchungs_ID;
	}
	public void setBuchungs_ID(int buchungs_ID) {
		Buchungs_ID = buchungs_ID;
	}
	public String getBenutzer_id() {
		return benutzer_id;
	}
	public void setBenutzer_id(String benutzer_id) {
		this.benutzer_id = benutzer_id;
	}
	//Getter
	public String getDatum() {
		return Datum;
	}

	public String getQuelle() {
		return Quelle;
	}

	public double getBetrag() {
		return Betrag;
	}
	//Setter
	public void setDatum(String datum) {
		Datum = datum;
	}

	public void setBetrag(double betrag) {
		Betrag = betrag;
	}

	public void setQuelle(String quelle) {
		Quelle = quelle;
	}
}
